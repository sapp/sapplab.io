How to build a new module
------------

When you are interested in creating a new module for the framework you should check out the example code at our [demo page](https://gitlab.com/sapp/demo).

The general procedure that we use throughout the modules is as follows:

- Argument parsing using JCommander (See: CommandOptionsDemo.java)
- Creation of input file for tool of interest using an RDF query and parser (See: Demo.java - loadRDF)
- Running the corresponding application ( See: Demo.java - run)
- Parsing the output file into a new (memory or disk) RDF database (See: Demo.java parser)
- Merge input HDT file with created RDF database to an output file (See: Demo.java save)


In a step by step overview we have selected IntelliJ for the development of the demo module.



![](images/Step1.png)
> An empty project has been created


![](images/Step2.png)
> Importing the demo module

![](images/Step3.png)
> Select the demo module

![](images/Step4.png)
> Import using Gradle

![](images/Step5.png)
> Make sure to have Gradle > 4.0 available either from your system or from gradlew

![](images/Step6.png)
> Select the Java SDK 1.8

![](images/Step7.png)

![](images/Step8.png)

![](images/Step9.png)

![](images/Step10.png)

![](images/Step11.png)
> Once all is setup the import can be finalised

![](images/Step12.png)
> IntelliJ will start building the package and retrieve all dependencies

![](images/Step13.png)
> As you can see SappGeneric is a dependency manager for SAPP containing Empusa and the API for the ontology

![](images/Step14.png)
> Once all is finished you can run the AppTest code to see what is happening.

![](images/Step15.png)
> We have made several simple test examples.
> 
> testApp() that just runs the application
> 
> testHDTFile() that shows how to query a file
> 
> testEndpoint that shows how to access a public endpoint

![](images/Step16.png)

![](images/Step17.png)