From binaries
------------

The simplest way to use SAPP is to download the binaries from: [http://download.systemsbiology.nl/sapp/dev/](http://download.systemsbiology.nl/sapp/dev/). 

You can download the jar files of interest and execute them using `java -jar SAPP-2.0.jar`.

From source
-----------
See the [development section](InstallationFromSource.md)


From docker
-----------
See the [docker section](docker.md)