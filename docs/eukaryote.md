## Genome annotation

!!! note
	It is possible that some modules might not work on your operating system. This is something we unfortunatly not always can resolve. If you encounter this please try to use the docker image as is provided from the docker section. All modules have been tested and should work on Ubuntu.
	
In this example we show how to annotate a eukaryotic genome or sequence of interest. First we need to obtain the sequence **(feel free to use your own genome fasta file)**. The genome we use in this tutorial is a *Drosophila melanogaster* obtained from [ENA](https://www.ebi.ac.uk/ena/data/view/GCA_000001215.4) and the sequence file (DMelanogaster.fasta) can be obtained [here](hhttps://www.ebi.ac.uk/ena/data/view/AE014298.5,AE014134.6,AE013599.5,AE014296.5,AE014297.3,AE014135.4,CP007106.1,KJ947872.2&display=fasta&download=fasta&filename=DMelanogaster.fasta). Please note that some browsers (safari) might change the file extension to .txt and this should be removed for this tutorial. 

###Getting help for a module
Type `java -jar <module name>.jar` to obtain the help information for a given module. For example for the  [Conversion.jar](http://download.systemsbiology.nl/sapp/Conversion.jar)  module: 

    java -jar Conversion.jar

	Usage: <main class> [options]
	  Options:    --help               (default: true)
	    -embl2rdf           input format is embl (default: false)
	    -fasta2rdf          input format is fasta (default: false)
	    -genbank2rdf        input format is genbank (default: false)
	    -gff2rdf            input format is GFF3 to create GBOL files (default: false)
	    -gtf2rdf            input format is GTF to create GBOL files (default: false)
	    -hdt2rdf, -rdf2hdt  Conversion of RDF to HDT or HDT to RDF (default: false)
	    -json2rdf           convert framed json to RDF (default: false)
	    -merge              Merging of HDT files (default: false)
	    -rdf2embl           input format is RDF to create EMBL files (default: false)
	    -rdf2fasta          input format is fasta (default: false)
	    -rdf2gtf            input format is RDF to create GTF files (default: false)
	    -rdf2json           convert from RDF to framed json (default: false)
	    -rdf2yaml           convert from RDF to framed yaml (mimicks genbank format) (default: false)
	    -yaml2rdf           convert from framed yaml (mimicks genbank format) to RDF (default: false)


###Converting your genome
---

After downloading we convert the genome sequence into the Semantic format SAPP uses. To do so we need to obtain the conversion module from [http://download.systemsbiology.nl/sapp/](http://download.systemsbiology.nl/sapp/).

Once the [Conversion.jar](http://download.systemsbiology.nl/sapp/Conversion.jar) has been downloaded you can start it from the command line. As we are currently have a FASTA file we need the `-fasta2rdf` option. No additional options will result in another help screen.

	java -jar Conversion.jar -fasta2rdf
	
	The following options are required: -i, -input -id, -identifier -org, -organism -o, -output 
	Usage: <main class> [options]
	  Options:    --help             (default: false)
	    -chromosome       Genome fasta file consists of complete chromosomes (default: false)
	    -codon            Codon table used for translating the genes (default: 0)
	    -contig           Genome fasta file consists of contigs (default: false)
	    -debug            Debug mode (default: false)
	    -fasta2rdf        Fasta to RDF conversion (default: true)
	    -format           hdt or ttl output format (default: hdt)
	    -gene             Fasta file consists of genes (default: false)
	    -genome           Fasta file consists of genome sequences (default: false)
	  * -i, -input        input FASTA file
	  * -id, -identifier  Unique identifier for your sample (e.g. GCA_000001
	  * -o, -output       output file in HDT GBOL format
	  * -org, -organism   Organism name
	    -protein          Fasta file consists of proteins (default: false)
	    -scaffold         Genome fasta file consists of scaffolds (default: false)
	    -stopcodon        Will not raise an exception when stop codons are detected during translation (default: false)
	    -topology         Describing wether sequence is circular or linear (only applicable for genome fasta files) (default: linear)
	    -translate        Translate the input sequence to protein (default: false)
	
	  * required parameter

This gives us again many options but specific to fasta conversion. The obligatory parameters (-i, -input -id, -identifier -org, -organism , -output) are required for all fasta conversions (genome, gene, protein). However some are optional and only required for specific conversion.

As we have a genome fasta file we use the following command

	java -jar Conversion.jar -fasta2rdf -input DMelanogaster.fasta -o DMelanogaster.hdt -genome -chromosome -id DMelanogaster -org "Drosophila melanogaster"
	
	2018-04-13 07:16:23 INFO  Logger:69 - Now performing FASTA analysis
	Saving to file...nces parsed: 8
	NTRIPLES: /var/folders/7s/1n66vsfj1c93hm09w2xz747c0000gn/T/tmp3038140076321997073.nt
	File converted in: 2 sec 434 ms 969 us
	HDTSAVE: DMelanogaster.hdt

The `DMelanogaster.hdt` should now be available in your folder and is the RDF database of the converted fasta file.

### Predicting genes
----

To populate the database further with for example gene predictions we can download the `genecaller.jar` from the [download page](http://download.systemsbiology.nl/sapp/)

To start the gene prediction program we start it up with the `java -jar` command.

	java -jar genecaller.jar 
	--help
	Usage: <main class> [options]
	  Options:    --help          (default: true)
	    -a, -augustus  Augustus for gene prediction (default: false)
	    -n, -naive     Naive approach for gene prediction (default: false)
	    -p, -prodigal  Prodigal for gene prediction (default: false)
	    
We currently support 2 different gene prediction programs and a naive translation of the genome sequence. As this is a eukaryotic genome we use augustus.

	java -jar genecaller.jar -augustus

	The following options are required: -c, -codon -o, -output 
	Usage: <main class> [options]
	Options:
			--help
		
		Default: false
		-a, -augustus
		Running in Augustus mode
		Default: true
	* -c, -codon
		Codon table used...
		Default: 0
		-debug
		Debug mode
		Default: false
		-e, -endpoint
		(In progress) SPARQL endpoint instead of input file
		-f, -format
		output in ttl or hdt format
		Default: hdt
		-i, -input
		Input file
		-n, -naive
		Running in naive translation mode
		Default: false
	* -o, -output
		Output file
		-pass, -password
		(In progress) Username for endpoint
		-p, -prodigal
		Running in Prodigal mode
		Default: false
		-s, -species
		Augustus species identifier
		Default: help
		-user, -username
		(In progress) Username for endpoint
		-v, -version
		Augustus version for mac (installed with homebrew)
		Default: 3.2.2_4

	* required parameter


Augustus requires that the a species is given using the `-s -species` option. As you cannot remember them all it will automatically provide you with the list when no species is given.

Once the commands are filled in the gene prediction should start:

    java -jar genecaller.jar -augustus -c 1 -i DMelanogaster.hdt -o DMelanogaster_augustus.hdt -s fly


	Connecting to HDT file
	DMelanogaster.hdt.fasta
	DMelanogaster.hdt.gff3
	2018-04-13 07:23:12 INFO  Logger:69 - Command: /usr/local/Cellar/augustus/3.2.2_4/bin/augustus --AUGUSTUS_CONFIG_PATH=/usr/local/Cellar/augustus/3.2.2_4/libexec/config/ --genemodel=complete --gff3=on --outfile=DMelanogaster.hdt.gff3 --species=fly DMelanogaster.hdt.fasta


	
And now you should be able to see the `DMelanogaster_augustus.hdt` file which contains the original data and the newly predicted genes with additional information.

!!! info
	The sentence 	`Could not read .hdt.index, Generating a new one.` indicates no error. This means that the database file that it currently uses has no index file present (yet).


### Functionally annotate enzymes using EnzDP
---

To functional annotate the genes with for example enzymes we can use the EnzDP module. This module needs to be retrieved from the download website and can be started directly. Please note that this module only works on linux systems. As the binaries from EnzDP are compiled for linux operating systems.

	java -jar EnzDP.jar 
	
	The following options are required: -i, -input -o, -output 
	Usage: <main class> [options]
	  Options:    -debug       Debug mode (default: false)
	  * -i, -input   Input HDT file
	  * -o, -output  Output HDT file
	
	  * required parameter

As this is a fully automated program only an input and output is required. Thus we run the program as followed:

	java -jar EnzDP.jar -input DMelanogaster_augustus.hdt -output DMelanogaster_augustus_enzdp.hdt
	
As it is a program which requires the EnzDP database dependency it will download the program once it is started with input and output arguments. As it is relatively large (1.9G zipped) it might take a while depending on your internet connection.

The database will be placed next to the jar file inside the `./Dependencies/Database/EnzDP` folder and is currently 11.68GB.

After downloading it will automatically continue and run the analysis.

	....
	NTRIPLES: /tmp/tmp1383585639358114106.nt
	HDT2RDF: /tmp/tmp1383585639358114106.nt
	File converted in: 1 sec 935 ms 378 us
	HDTSAVE: DMelanogaster_augustus_enzdp.hdt
	

Now you should have a file called `DMelanogaster_augustus_enzdp.hdt` which contains the genome sequence, the predicted genes and the functional annotation from EnzDP in a single semantic database.


## Now you can move to the Query section to extract features from the database

