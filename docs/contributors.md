# Code Contributors

The following people contributed to the development of the Semantic Annotation Platform with Provenance.

## Main developers

* Jasper Koehorst
* Jesse van Dam

## Module contributors

* Benoit Carreres - Transcriptomics / GeneCaller / Conversion
* Arne B. Gjuvsland - Phobius

## Other contributors

* Mathijs Kattenberg (Spark support)

## Students

* William Sies (Continues integration and test cases)
* Marco Roelfes (QTL and VCF development)
