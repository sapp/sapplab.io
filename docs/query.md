## Querying your database
---

To directly query this database we have developed a small query module which can interact directly with one or more HDT files.

The package can be called via `java -jar SAPP-2.0.jar -sparql` and upon execution you get multiple options:

	The following option is required: [-query]
	Usage: <main class> [options]
	Options:
		--help
		Default: false
		-debug
		Debug mode
		Default: false
		-f, -format
		Output format, csv / tsv / json / xml
		Default: tsv
		-i, -input
		HDT input file or folder (regex file selection is possible) for 
		querying, multiple folders can be given using , as separator
		-jobs
		Number of jobs to run in parallel
		Default: 4
		-o, -output
		Query result file, otherwise stdout
	* -query
		SPARQL Query or FILE containing the query to execute
		-r, -recursive
		Iteratively goes through sub directories searching for HDT files
		Default: false
		-sparql
		Application: SPARQL query interface
		Default: true

	* required parameter

To perform a simple query to obtain the total number of genes one can execute the following command:

	java -jar SAPP-2.0.jar -sparql -i StrepPyogenes5005.hdt -query "PREFIX gbol:<http://gbol.life/0.1/> SELECT (COUNT(?gene) AS ?total) WHERE { ?gene a gbol:Chromosome }"

	2023-07-20 22:12:59,281 [main] INFO  SPARQL - Processing sapp_sparql_11140858766549014643.tsv
	?total
	1

### More complex queries

----

More complex queries such as obtaining all predicted enzymes with the corresponding likelihood-score can be achieved as follow:

    java -jar SAPP-2.0.jar -sparql -i StrepPyogenes5005_prodigal_enzdp.hdt -query "PREFIX gbol:<http://gbol.life/0.1/> SELECT ?accession ?like WHERE { ?protein a gbol:Protein . ?protein gbol:feature ?feature . ?feature gbol:xref ?xref . ?xref gbol:accession ?accession . ?feature gbol:provenance ?prov . ?prov gbol:annotation ?annot . ?annot gbol:likelihoodscore ?like . FILTER(?like > 0.01) }"
    
    They query formatted:
    
    PREFIX gbol:<http://gbol.life/0.1/> 
    SELECT ?accession ?like 
    WHERE { 
    	?protein a gbol:Protein . 
    	?protein gbol:feature ?feature . 
    	?feature gbol:xref ?xref . 
    	?xref gbol:accession ?accession . 
    	?feature gbol:provenance ?prov . 
    	?prov gbol:annotation ?annot . 
    	?annot gbol:likelihoodscore ?like . 
    	FILTER(?like > 0.01)
    }
	
	1.-.-.-,0.902
	1.-.-.-,1.0
	1.-.-.-,0.219
	1.-.-.-,0.191
	1.-.-.-,0.095
	1.-.-.-,0.199
	1.-.-.-,0.726
	1.-.-.-,0.116
	1.-.-.-,0.16
	....
	6.4.1.1,0.368
	6.4.1.2,1.0
	6.4.1.2,1.0
	6.4.1.2,0.625
	6.5.1.2,0.15
	6.5.1.2,1.0

You can adjust the restriction easily by adjusting the filter value to for example 0.8. and you should see a decrease in results as you are now more strict.
