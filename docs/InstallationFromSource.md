*When you are planning to modify the source code*

The core of SAPP is based on JAVA 11 using GRADLE dependencies. To know more about Gradle have a look at [https://gradle.org](https://gradle.org).

All modules have been tested through the GitLab CI.

Databases as well as certain software packages ( e.g. InterProScan) are required to be installed on the system of execution.

In summary:

*  Java 11
*  GIT (tested with 2.13.0 linux and 2.13.3 mac)
*  Gradle (Tested with 4.0.1)

### Mac / Linux
To install java, git and gradle efficiently, http://sdkman.io is the preferred method.

## Compilation

To compile the code the default approach is to use the gradle build system. For a quick approach `./gradlew build -x test` will compile the code and skip the tests. The executable jar file will be located in `build/libs/`.