An example input file (StrepPyogenes5005.fasta) can be obtained [here](https://www.ebi.ac.uk/ena/data/view/CP000017.2&display=fasta&download=fasta&filename=StrepPyogenes5005.fasta). The output of the conversion and annotation of StrepPyogenes5005.fasta can be found [in binary format (hdt)](https://sapp.gitlab.io/files/StrepPyogenes5005_prodigal_enzdp.hdt) or [plain text (ttl)](https://sapp.gitlab.io/files/StrepPyogenes5005_prodigal_enzdp.hdt.ttl.gz)

The core of SAPP is based on RDF technology. It allows to convert and annotate existing annotation files and perform various analysis at a significant pace. The output format of most modules is therefore in this format.

We have chosen to use the HDT (Header, Dictionary, Triple) format as default input* and output throughout the framework. More information about this format can be found at [http://www.rdfhdt.org](http://www.rdfhdt.org). The main benefit about this format in contrast to other resources is that it can be used directly without the need to load it into a local triple store and that it consists of a single file which makes distribution and including it into workflows much simpler. 

As not all big triple stores such as GraphDB or BlazeGraph do not support this format, the generated files can be converted to RDF (Turtle) using the conversion module using the following command:

java -jar conversion.jar -hdt2rdf -i **input** -o **output.ttl**

When RDF files were given or extracted from a triple store they need to be converted back to HDT to be used within the framework which can be achieved using the -rdf2hdt command with the corresponding -i and -o.





	Except for the conversion module which also allows to take in 
	genbank, embl, fasta and GFF format which in turn are converted to 
	the HDT format`
