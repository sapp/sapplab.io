# Frequently Asked Queries

If you know of a nice query or would like to place a request feel free to place it in the [documentation](https://gitlab.com/sapp/sapp.gitlab.io/blob/master/docs/FAQ.md)

## The following queries have been tested with
	
	VALUES ?sample { <http://gbol.life/0.1/GCA_000003135/sample> }

## After conversion queries

These queries can be applied directly on converted sequence files with or for some also without annotation.
 
### GC content

	PREFIX gbol:<http://gbol.life/0.1/>
	SELECT DISTINCT ?sample (SUM(?gcSize) / SUM(?total) * 100 AS ?x) 
	WHERE { 
		?sample a gbol:Sample .
	    ?dnaobject gbol:sample ?sample .
	    ?dnaobject gbol:sequence ?sequence .
	    BIND(REPLACE(?sequence, "[AT]","") AS ?gc)
	    BIND(STRLEN(?sequence) AS ?total)
	    BIND(STRLEN(?gc) AS ?gcSize)
	} GROUP BY ?sample

### Nucleotide composition

	PREFIX gbol:<http://gbol.life/0.1/>
	SELECT DISTINCT ?sample (SUM(?size) AS ?totalSize) (SUM(?alength) AS ?aSize) (SUM(?tlength) AS ?tSize) (SUM(?clength) AS ?cSize) (SUM(?glength) AS ?gSize)
	WHERE { 
		?sample a gbol:Sample .
	    ?dnaobject gbol:sample ?sample .
	    ?dnaobject gbol:sequence ?sequence .
	    BIND(STRLEN(?sequence) AS ?size)
	    BIND(?size - STRLEN(REPLACE(?sequence, "A",""))AS ?alength)
	    BIND(?size - STRLEN(REPLACE(?sequence, "T",""))AS ?tlength)
	    BIND(?size - STRLEN(REPLACE(?sequence, "G",""))AS ?glength)
	    BIND(?size - STRLEN(REPLACE(?sequence, "C",""))AS ?clength)
	} GROUP BY ?sample

### Number of genes

	PREFIX gbol:<http://gbol.life/0.1/>
	SELECT DISTINCT ?sample (COUNT(?gene) AS ?genes)
	WHERE { 
		?sample a gbol:Sample .
	    ?dnaobject gbol:sample ?sample .
	    ?dnaobject gbol:feature ?gene .
	    ?gene a gbol:Gene .
	} GROUP BY ?sample
	
### Number of CDS

	PREFIX gbol:<http://gbol.life/0.1/>
	SELECT DISTINCT ?sample (COUNT(DISTINCT(?cds)) AS ?cdss)
	WHERE { 
		?sample a gbol:Sample .
	    ?dnaobject gbol:sample ?sample .
	    ?dnaobject gbol:feature ?gene .
	    ?gene a gbol:Gene .
	    ?gene gbol:transcript ?transcript .
	    ?transcript gbol:feature ?cds .
	    ?cds a gbol:CDS .
	} GROUP BY ?sample

### Number of RNA types

	PREFIX gbol:<http://gbol.life/0.1/>
	SELECT DISTINCT ?sample ?type (COUNT(?feat) AS ?rnas)
	WHERE { 
		?sample a gbol:Sample .
	    ?dnaobject gbol:sample ?sample .
	    ?dnaobject gbol:feature ?gene .
	    ?gene a gbol:Gene .
	    ?gene gbol:transcript ?feat  .
	    ?feat a ?type .
	} GROUP BY ?sample ?type

### Number of unique proteins (A good quality measurement analysis)

	PREFIX gbol:<http://gbol.life/0.1/>
	SELECT DISTINCT ?sample (COUNT(DISTINCT(?protein)) AS ?dproteins) (COUNT(?protein) AS ?tproteins)
	WHERE { 
		?sample a gbol:Sample .
	    ?dnaobject gbol:sample ?sample .
	    ?dnaobject gbol:feature ?gene .
	    ?gene a gbol:Gene .
	    ?gene gbol:transcript ?transcript  .
	    ?transcript gbol:feature ?cds .
	    ?cds gbol:protein ?protein .
	} GROUP BY ?sample ?type
	
## Annotation queries

When annotation files have external references using the Xref property they can be directly queried. However keep in mind that no provenance information is available in these files.

### Number of genes with an EC number

	PREFIX gbol:<http://gbol.life/0.1/>
	SELECT DISTINCT ?sample (COUNT(DISTINCT(?gene)) AS ?haveEnzyme)
	WHERE { 
		?sample a gbol:Sample .
	    ?dnaobject gbol:sample ?sample .
	    ?dnaobject gbol:feature ?gene .
	    ?gene a gbol:Gene .
	    ?gene gbol:transcript ?transcript  .
	    ?transcript gbol:feature ?cds .
	    ?cds gbol:protein ?protein .
	    ?protein gbol:xref ?xref .
	    ?xref gbol:accession ?acc .
	    ?xref gbol:db <http://gbol.life/0.1/db/ec> .
	} GROUP BY ?sample

### Which external references are available for my genome?

	PREFIX gbol:<http://gbol.life/0.1/>
	SELECT DISTINCT ?db
	WHERE { 
	    ?xref a gbol:XRef .
	    ?xref gbol:db ?db .
	}

## Tool specific queries

### All genes with a confidence value > 95 according to prodigal gene prediction

	PREFIX gbol:<http://gbol.life/0.1/>
	SELECT DISTINCT ?sample ?gene ?conf
	WHERE { 
		?sample a gbol:Sample .
	    ?dnaobject gbol:sample ?sample .
	    ?dnaobject gbol:feature ?gene .
	    ?gene a gbol:Gene .
	    ?gene gbol:provenance ?prov .
	    ?prov gbol:annotation ?annot .
	    ?annot gbol:conf ?conf .
	    FILTER(?conf > 95)
	}



## Request section

You can place your request here from within the [documentation](https://gitlab.com/sapp/sapp.gitlab.io/blob/master/docs/FAQ.md) or contact us directly.
