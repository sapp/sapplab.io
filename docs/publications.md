# Publications

When using SAPP please notify us so that we can add you to the list.

```{note}
SAPP: functional genome annotation and analysis through a semantic framework using FAIR principles.

Koehorst, J. J., van Dam, J. C., Saccenti, E., Martins dos Santos, V. A., Suarez-Diez, M., & Schaap, P. J. (2018). Bioinformatics, 34(8), 1401-1403.
```

```{note}
When referring to the SAPP ontology please cite"

The Empusa code generator and its application to GBOL, an extendable ontology for genome annotation.

van Dam, J. C., Koehorst, J. J., Vik, J. O., Martins dos Santos, V. A., Schaap, P. J., & Suarez-Diez, M. (2019).  Scientific data, 6(1), 254.
```

<table>

<tr valign="top">
<td align="right" class="bibtexnumber">
[<a name="sapp">1</a>]
</td>
<td class="bibtexitem">
Jasper&nbsp;J. Koehorst, Jesse&nbsp;C.J. van Dam, Edoardo Saccenti, Vitor&nbsp;A.P.
  Martins&nbsp;dos Santos, Maria Suarez-Diez, and Peter&nbsp;J. Schaap.
 SAPP: functional genome annotation and analysis through a semantic
  framework using fair principles.
 <em>TBA</em>, 0(0):0, 2017.
[&nbsp;<a href="test.bib_bib.html#sapp">bib</a>&nbsp;]

</td>
</tr>

<tr valign="top">
<td align="right" class="bibtexnumber">
[<a name="ouwerkerk2017complete">2</a>]
</td>
<td class="bibtexitem">
Janneke&nbsp;P Ouwerkerk, Jasper&nbsp;J Koehorst, Peter&nbsp;J Schaap, Jarmo Ritari, Lars
  Paulin, Clara Belzer, and Willem&nbsp;M de&nbsp;Vos.
 Complete genome sequence of akkermansia glycaniphila strain pytt, a
  mucin-degrading specialist of the reticulated python gut.
 <em>Genome announcements</em>, 5(1):e01098-16, 2017.
[&nbsp;<a href="test.bib_bib.html#ouwerkerk2017complete">bib</a>&nbsp;]

</td>
</tr>

<tr valign="top">
<td align="right" class="bibtexnumber">
[<a name="timmers2017reverse">3</a>]
</td>
<td class="bibtexitem">
Peer&nbsp;HA Timmers, Cornelia&nbsp;U Welte, Jasper&nbsp;J Koehorst, Caroline&nbsp;M Plugge,
  Mike&nbsp;SM Jetten, and Alfons&nbsp;JM Stams.
 Reverse methanogenesis and respiration in methanotrophic archaea.
 <em>Archaea</em>, 2017, 2017.
[&nbsp;<a href="test.bib_bib.html#timmers2017reverse">bib</a>&nbsp;]

</td>
</tr>

<tr valign="top">
<td align="right" class="bibtexnumber">
[<a name="kamminga2017persistence">4</a>]
</td>
<td class="bibtexitem">
Tjerko Kamminga, Jasper&nbsp;J Koehorst, Paul Vermeij, Simen-Jan Slagman, Vitor
  AP&nbsp;Martins dos Santos, Jetta&nbsp;JE Bijlsma, and Peter&nbsp;J Schaap.
 Persistence of functional protein domains in mycoplasma species and
  their role in host specificity and synthetic minimal life.
 <em>Frontiers in cellular and infection microbiology</em>, 7, 2017.
[&nbsp;<a href="test.bib_bib.html#kamminga2017persistence">bib</a>&nbsp;]

</td>
</tr>

<tr valign="top">
<td align="right" class="bibtexnumber">
[<a name="peng2017concurrent">5</a>]
</td>
<td class="bibtexitem">
Peng Peng, Ying Zheng, Jasper&nbsp;J Koehorst, Peter&nbsp;J Schaap, Alfons&nbsp;JM Stams,
  Hauke Smidt, and Siavash Atashgahi.
 Concurrent haloalkanoate degradation and chlorate reduction by
  pseudomonas chloritidismutans aw-1t.
 <em>Applied and Environmental Microbiology</em>, 83(12):e00325-17,
  2017.
[&nbsp;<a href="test.bib_bib.html#peng2017concurrent">bib</a>&nbsp;]

</td>
</tr>

<tr valign="top">
<td align="right" class="bibtexnumber">
[<a name="van2017interoperable">6</a>]
</td>
<td class="bibtexitem">
Jesse&nbsp;CJ van Dam, Jasper Jan&nbsp;J Koehorst, Jon&nbsp;Olav Vik, Peter&nbsp;J Schaap, and
  Maria Suarez-Diez.
 Interoperable genome annotation with gbol, an extendable
  infrastructure for functional data mining.
 <em>bioRxiv</em>, page 184747, 2017.
[&nbsp;<a href="test.bib_bib.html#van2017interoperable">bib</a>&nbsp;]

</td>
</tr>

<tr valign="top">
<td align="right" class="bibtexnumber">
[<a name="koehorst2016protein">7</a>]
</td>
<td class="bibtexitem">
Jasper&nbsp;J Koehorst, Edoardo Saccenti, Peter&nbsp;J Schaap, Vitor AP&nbsp;Martins dos
  Santos, and Maria Suarez-Diez.
 Protein domain architectures provide a fast, efficient and scalable
  alternative to sequence-based methods for comparative functional genomics.
 <em>F1000Research</em>, 5, 2016.
[&nbsp;<a href="test.bib_bib.html#koehorst2016protein">bib</a>&nbsp;]

</td>
</tr>

<tr valign="top">
<td align="right" class="bibtexnumber">
[<a name="bosma2016complete">8</a>]
</td>
<td class="bibtexitem">
Elleke&nbsp;F Bosma, Jasper&nbsp;J Koehorst, Sacha&nbsp;AFT van Hijum, Bernadet Renckens,
  Bastienne Vriesendorp, Antonius&nbsp;HP van&nbsp;de Weijer, Peter&nbsp;J Schaap, Willem&nbsp;M
  de&nbsp;Vos, John van&nbsp;der Oost, and Richard van Kranenburg.
 Complete genome sequence of thermophilic bacillus smithii type strain
  dsm 4216 t.
 <em>Standards in genomic sciences</em>, 11(1):52, 2016.
[&nbsp;<a href="test.bib_bib.html#bosma2016complete">bib</a>&nbsp;]

</td>
</tr>

<tr valign="top">
<td align="right" class="bibtexnumber">
[<a name="koehorst2016comparison">9</a>]
</td>
<td class="bibtexitem">
Jasper&nbsp;J Koehorst, Jesse&nbsp;CJ Van&nbsp;Dam, Ruben&nbsp;GA Van&nbsp;Heck, Edoardo Saccenti, Vitor
  AP&nbsp;Martins Dos&nbsp;Santos, Maria Suarez-Diez, and Peter&nbsp;J Schaap.
 Comparison of 432 pseudomonas strains through integration of genomic,
  functional, metabolic and expression data.
 <em>Scientific reports</em>, 6, 2016.
[&nbsp;<a href="test.bib_bib.html#koehorst2016comparison">bib</a>&nbsp;]

</td>
</tr>

<tr valign="top">
<td align="right" class="bibtexnumber">
[<a name="saccenti2015assessing">10</a>]
</td>
<td class="bibtexitem">
Edoardo Saccenti, David Nieuwenhuijse, Jasper&nbsp;J Koehorst, Vitor AP&nbsp;Martins dos
  Santos, and Peter&nbsp;J Schaap.
 Assessing the metabolic diversity of streptococcus from a protein
  domain point of view.
 <em>PloS one</em>, 10(9):e0137908, 2015.
[&nbsp;<a href="test.bib_bib.html#saccenti2015assessing">bib</a>&nbsp;]

</td>
</tr>

<tr valign="top">
<td align="right" class="bibtexnumber">
[<a name="van2015rdf2graph">11</a>]
</td>
<td class="bibtexitem">
Jesse&nbsp;CJ van Dam, Jasper&nbsp;J Koehorst, Peter&nbsp;J Schaap, Vitor AP&nbsp;Martins dos
  Santos, and Maria Suarez-Diez.
 Rdf2graph a tool to recover, understand and validate the ontology of
  an rdf resource.
 <em>Journal of biomedical semantics</em>, 6(1):39, 2015.
[&nbsp;<a href="test.bib_bib.html#van2015rdf2graph">bib</a>&nbsp;]

</td>
</tr>

<tr valign="top">
<td align="right" class="bibtexnumber">
[<a name="visser2014genome">12</a>]
</td>
<td class="bibtexitem">
Michael Visser, Sofiya&nbsp;N Parshina, Joana&nbsp;I Alves, Diana&nbsp;Z Sousa, In&ecirc;s&nbsp;AC
  Pereira, Gerard Muyzer, Jan Kuever, Alexander&nbsp;V Lebedinsky, Jasper&nbsp;J
  Koehorst, Petra Worm, et&nbsp;al.
 Genome analyses of the carboxydotrophic sulfate-reducers
  desulfotomaculum nigrificans and desulfotomaculum carboxydivorans and
  reclassification of desulfotomaculum caboxydivorans as a later synonym of
  desulfotomaculum nigrificans.
 <em>Standards in genomic sciences</em>, 9(3):655, 2014.
[&nbsp;<a href="test.bib_bib.html#visser2014genome">bib</a>&nbsp;]

</td>
</tr>

<tr valign="top">
<td align="right" class="bibtexnumber">
[<a name="worm2014genomic">13</a>]
</td>
<td class="bibtexitem">
Petra Worm, Jasper&nbsp;J Koehorst, Michael Visser, Vicente&nbsp;T Sedano-N&uacute;&ntilde;ez,
  Peter&nbsp;J Schaap, Caroline&nbsp;M Plugge, Diana&nbsp;Z Sousa, and Alfons&nbsp;JM Stams.
 A genomic view on syntrophic versus non-syntrophic lifestyle in
  anaerobic fatty acid degrading communities.
 <em>Biochimica et Biophysica Acta (BBA)-Bioenergetics</em>,
  1837(12):2004-2016, 2014.
[&nbsp;<a href="test.bib_bib.html#worm2014genomic">bib</a>&nbsp;]

</td>
</tr>

<tr valign="top">
<td align="right" class="bibtexnumber">
[<a name="pengsimultaneous">14</a>]
</td>
<td class="bibtexitem">
Peng Peng, Ying Zheng, Jasper&nbsp;J Koehorst, Peter&nbsp;J Schaap, Alfons&nbsp;JM Stams,
  Hauke Smidt, and Siavash Atashgahi.
 Simultaneous degradation of organic and inorganic chlorinated
  compounds by pseudomonas chloritidismutans aw-1t.
[&nbsp;<a href="test.bib_bib.html#pengsimultaneous">bib</a>&nbsp;]

</td>
</tr>

<tr valign="top">
<td align="right" class="bibtexnumber">
[<a name="vissergenome">15</a>]
</td>
<td class="bibtexitem">
Michael Visser, Sofiya&nbsp;N Parshina, Joana&nbsp;I Alves, Diana&nbsp;Z Sousa, In&ecirc;s&nbsp;AC
  Pereira, Gerard Muyzer, Jan Kuever, Alexander&nbsp;V Lebedinsky, Jasper&nbsp;J
  Koehorst, Petra Worm, et&nbsp;al.
 Genome analyses of the carboxydotrophic sulfate-reducers.
[&nbsp;<a href="test.bib_bib.html#vissergenome">bib</a>&nbsp;]

</td>
</tr>
</table>

*[And more](https://scholar.google.com/scholar?cites=2596566845932387739&as_sdt=2005&sciodt=0,5&hl=en)*