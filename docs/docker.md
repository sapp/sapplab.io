# Docker

###Requirements
To be able to use the SAPP docker image, docker is required. Docker can be obtained from: [https://www.docker.com/](https://www.docker.com/)

!!! info
	Default of 2 gigabyte memory by docker might not be sufficient for all packages. Please increase this when using for example InterProScan.


## Docker image for SAPP

We assume that when you follow this tutorial you start from the following directory in your home folder `SAPP`
In this folder create a data folder called **Data**.
Obtain the following [file](ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/005/845/GCF_000005845.2_ASM584v2/GCF_000005845.2_ASM584v2_genomic.fna.gz) as it will be used as an example. Place this file unpacked inside the **Data** folder.

To summarize:

* Create a SAPP folder somewhere on your system
* Within the SAPP folder create a Data folder
* Place the GCF_000005845.2_ASM584v2_genomic.fna ***!unpacked!*** inside the Data folder
* Start the tutorial from the SAPP folder not the Data folder

### Tutorial

We have developed a docker image with the jar files and dependency files. The commandline interface can be started by running:

    # Docker may require root priviliges, then use `sudo docker run`:
    docker run -it -v "$(pwd)"/Data:/SAPP/Data docker-registry.wur.nl/m-unlock/docker/sapp:2.0 /bin/bash

This will start the docker in interactive mode. Inside this container you have root privilages and you can execute the SAPP 2.0 jar file directly.

You can now use SAPP using the standard java call command e.g. `java -jar SAPP-2.0.jar` which will gives you the help function. A more extensive tutorial is available in the tutorial section.

```
> java -jar SAPP-2.0.jar 
2023-07-20 19:36:09,481 [main] WARN  App - No arguments given

Usage: <main class> [options]
  Options:
    --help
      Requesting help message
      Default: true
    -convert
      Conversion:  RDF to any other format (TTL/RDF/HDT/etc...) or folder (if 
      no rdf extension is given)
      Default: false
    -diamond
```

This will show you all the options for this module and if you then convert the fasta file to RDF (HDT) using the following command. This will create an output file inside directly inside the Data folder.

    # This will create a RDF database for the given FASTA file for further annotation
    java -jar SAPP-2.0.jar -fasta2rdf -genome \
     -i Data/GCF_000005845.2_ASM584v2_genomic.fna \
     -o Data/GCF_000005845.2_ASM584v2_genomic.fna.hdt \
     -id GCF_000005845.2_ASM584v2 -codon 11 -chromosome

!!! tip
    To make the HDT file accessible for other triple stores you can convert it to TURTLE by using:

    java -jar SAPP-2.0.jar -convert \
     -i Data/GCF_000005845.2_ASM584v2_genomic.fna.hdt \
     -o Data/GCF_000005845.2_ASM584v2_genomic.fna.ttl

To exit the docker environment you can type `exit`.

!!! tip
    Usefull docker commands for the host (not inside the docker):
    # Overview of current running dockers
    docker ps
    # Kill a running docker
    docker stop <docker_id>
