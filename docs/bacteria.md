# Microbial Genome annotation

In this example we show how to annotate a genome or sequence of interest. First we need to obtain the sequence **(feel free to use your own genome fasta file)**. The genome we use in this tutorial is a *Streptococcus pyogenes MGAS5005* obtained from [ENA](https://www.ebi.ac.uk/ena/data/view/GCA_000011765.2) and the sequence file (StrepPyogenes5005.fasta) can be obtained [here](https://www.ebi.ac.uk/ena/data/view/CP000017.2&display=fasta&download=fasta&filename=StrepPyogenes5005.fasta). Please note that some browsers (safari) might change the file extension to .txt and this should be removed for this tutorial. The results of the conversion and annotation of StrepPyogenes5005.fasta can be found [here (hdt)](https://sapp.gitlab.io/files/StrepPyogenes5005_prodigal_enzdp.hdt) or [here (ttl)](https://sapp.gitlab.io/files/StrepPyogenes5005_prodigal_enzdp.hdt.ttl.gz)

## Getting help

Type `java -jar SAPP-2.0.jar` to obtain the help information for all the options SAPP-2.0 provides.

```bash
java -jar SAPP-2.0.jar
Usage: <main class> [options]
  Options:
    --help
      Requesting help message
      Default: true
    -convert
      Conversion:  RDF to any other format (TTL/RDF/HDT/etc...) or folder (if 
      no rdf extension is given)
      Default: false
    -diamond
      Application: Running in diamond blast mode
      Default: false
    -eggnog
      Application: Running in EggNOG mode
      Default: false
    -embl2rdf
      Conversion: input format is embl
      Default: false
    -fasta2rdf
      Conversion: input format is fasta
      Default: false
    -genbank2rdf
      Conversion: input format is genbank
      Default: false
    -gff2rdf
      Conversion: input format is GFF3 to create GBOL files
      Default: false
    -gtf2rdf
      Conversion: input format is GTF to create GBOL files
      Default: false
    -infernal
      Application: Running in Infernal mode
      Default: false
    -interpro
      Application: Running in InterProScan mode
      Default: false
    -kofamscan
      Application: Running in KoFamScan mode
      Default: false
    -merge
      Conversion: Merging of HDT files
      Default: false
    -prodigal
      Application: Running in Prodigal mode
      Default: false
    -rdf2embl
      Conversion: input format is RDF to create EMBL files
      Default: false
    -rdf2fasta
      Conversion: input format is fasta
      Default: false
    -rdf2gtf
      Conversion: input format is RDF to create GTF files
      Default: false
    -reduce
      Conversion: Reduces the genomic HDT file by removing sequence 
      information 
      Default: false
    -sparql
      Application: SPARQL query interface
      Default: false

  * required parameter
```

## Converting your genome

After downloading we convert the genome sequence into the Semantic format SAPP uses. In this case we need to transform the FASTA file into a RDF file using the `-fasta2rdf` function.

This gives us again many options but specific to fasta conversion. The following options are required: [-id | -identifier], [-o | -output], [-codon]. However some are optional and only required for specific conversion.

As we have a genome fasta file we use the following command

```java
	java -jar SAPP-2.0.jar -fasta2rdf -input StrepPyogenes5005.fasta -o StrepPyogenes5005.ttl -genome -chromosome -id StrepPyogenes5005 -codon 11
	
2024-02-08 16:00:47,990 [main] INFO  App - Performing conversion
2024-02-08 16:00:48,603 [main] INFO  Fasta - Number of files to process: 1
2024-02-08 16:00:48,710 [main] DEBUG Fasta - Comment: ENA|CP000017|CP000017.2 Streptococcus pyogenes MGAS5005, complete genome.
2024-02-08 16:00:48,712 [main] DEBUG Genome - Submitter Accession: ENA/CP000017/CP000017.2
2024-02-08 16:00:48,842 [main] INFO  Fasta - Saving all stores to StrepPyogenes5005.hdt
2024-02-08 16:00:48,843 [main] INFO  Output - Saving all stores to StrepPyogenes5005.hdt
2024-02-08 16:00:48,844 [main] WARN  Output - HDT is not supported for this function, converting to turtle instead: StrepPyogenes5005.hdt -> StrepPyogenes5005.ttl
2024-02-08 16:00:48,970 [main] INFO  Output - Converting to HDT, warning need to load the entire file into memory
2024-02-08 16:00:49,067 [main] INFO  Output - Saving HDT to /Volumes/Git/ssb/sapp.gitlab.io/StrepPyogenes5005.hdt
2024-02-08 16:00:49,188 [main] INFO  App - Conversion completed
```

The `StrepPyogenes5005.hdt` should now be available in your folder and is the RDF database of the converted fasta file.

## Predicting genes

To populate the database further with for example gene predictions we can use the `gene prediction` module.

To start the gene prediction program we start it up with the `java -jar SAPP-2.0.jar -prodigal` command.

```java
	java -jar SAPP-2.0.jar -prodigal
	The following options are required: [-o | -output], [-i | -input]
	Usage: <main class> [options]
	Options:
		--help
		Default: false
		-c, -codon
		Codon table used (overwrites genetic table in RDF data)
		Default: -1
		-debug
		Debug mode
		Default: false
		-faa
		Protein fasta file (skips prodigal prediction)
		-ffa
		Nucleotide fasta file (skips prodigal prediction)
		-gff
		Gene gff file (skips prodigal prediction)
	* -i, -input
		Input file
		-m, -meta
		meta genome analysis
		Default: false
	* -o, -output
		Output file
		-prodigal
		Running in Prodigal mode
		Default: true

	* required parameter
```

Once the commands are filled in the gene prediction should be finished relatively fast.

```java
java -jar SAPP-2.0.jar -prodigal -i StrepPyogenes5005.hdt -o StrepPyogenes5005.prodigal.ttl

2024-02-08 16:02:28,743 [main] INFO  App - Performing Prodigal analysis
2024-02-08 16:02:29,237 [main] DEBUG Input - Loading /Volumes/Git/ssb/sapp.gitlab.io/StrepPyogenes5005.hdt
2024-02-08 16:02:29,237 [main] INFO  Input - Loading 1 rdf files
2024-02-08 16:02:29,237 [main] INFO  Input - Loading files into RDF_361548110
Predicate Bitmap in 253 us
Count predicates in 280 us
Count Objects in 55 us Max was: 1
Bitmap in 13 us
Object references in 64 us
Sort object sublists in 13 us
Count predicates in 49 us
Index generated in 407 us
Index generated and saved in 5 ms 938 us
2024-02-08 16:02:30,267 [main] INFO  FASTA - Obtaining sequences
2024-02-08 16:02:30,361 [main] INFO  FASTA - Found 1 sample(s)
2024-02-08 16:02:30,361 [main] INFO  FASTA - Writing to file
2024-02-08 16:02:30,361 [main] INFO  FASTA - Creating FASTA file for: StrepPyogenes5005
2024-02-08 16:02:30,378 [main] INFO  FASTA - FASTA file(s) created
binaries/mac/prodigal/2.6.3/prodigal
/Volumes/Git/ssb/sapp.gitlab.io/tmp/prodigal
Creating file: /Volumes/Git/ssb/sapp.gitlab.io/tmp/prodigal
2024-02-08 16:02:30,489 [main] INFO  Prodigal - Obtaining codon information from RDF file
2024-02-08 16:02:30,491 [main] INFO  Prodigal - Translation table set to 11
2024-02-08 16:02:30,491 [main] INFO  Prodigal - Analysing: StrepPyogenes5005
2024-02-08 16:02:30,494 [main] INFO  Prodigal - /Volumes/Git/ssb/sapp.gitlab.io/tmp/prodigal -q -f gff -o StrepPyogenes5005_genecaller.gff -a StrepPyogenes5005_proteins.fasta -p single -g 11 -t StrepPyogenes5005_genecaller.trn -d StrepPyogenes5005_nucleotide.fasta -i /Volumes/Git/ssb/sapp.gitlab.io/StrepPyogenes5005
2024-02-08 16:02:33,569 [main] INFO  Prodigal - /Volumes/Git/ssb/sapp.gitlab.io/tmp/prodigal -q -f gff -o StrepPyogenes5005_genecaller.gff -a StrepPyogenes5005_proteins.fasta -p single -g 11 -t StrepPyogenes5005_genecaller.trn -d StrepPyogenes5005_nucleotide.fasta -i /Volumes/Git/ssb/sapp.gitlab.io/StrepPyogenes5005
2024-02-08 16:02:35,191 [main] INFO  Prodigal - Building gff dictionary
2024-02-08 16:02:35,262 [main] INFO  Prodigal - Building gene dictionary
2024-02-08 16:02:35,349 [main] INFO  Prodigal - Building protein dictionary
2024-02-08 16:02:35,383 [main] INFO  Prodigal - Parsing gene dictionary
2024-02-08 16:02:41,479 [main] INFO  App - Prodigal analysis completed
```

And now you should be able to see the `StrepPyogenes5005.prodigal.ttl` file which contains the original data and the newly predicted genes with additional information.

## Functionally annotate protein coding genes

