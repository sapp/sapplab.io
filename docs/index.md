# SAPP

## Semantic Annotation Platform with Provenance

SAPP is a Semantic genome Annotation Platform with Provenance and is designed on the basis of Semantic Web. The platform and corresponding modules allows you to annotate genomes of various qualities linked to a full chain of data provenance. Resulting is an annotated genome in the RDF data model which you can query and analyse using SPARQL. Various modules are available which allows you to compare, annotate and visualise genomes and export your annotations to various standard genome annotation formats.

For a global overview of the modules available start the application without any arguments.

## About us

SAPP is an initiative by the [Laboratory of Systems and Synthetic Biology](http://www.wur.nl/en/Expertise-Services/Chair-groups/Agrotechnology-and-Food-Sciences/Laboratory-of-Systems-and-Synthetic-Biology.htm) from [Wageningen University & Research](http://wur.nl/en).

### Funding

This work has received funding from the Research Council of Norway, No.  248792 (DigiSal) and from the European Union FP7 and H2020 under grant agreements No. 305340 (INFECT), No. 635536 (EmPowerPutida) and No. 634940 (MycoSynVac).

### Contact

* Please submit bug reports and feature suggestions, [here...](https://gitlab.com/sapp/sapp/issues)

#### When using SAPP please cite

    SAPP: functional genome annotation and analysis through a semantic framework using FAIR principles<br>
    Koehorst, Jasper J. and van Dam, Jesse C.J. and Saccenti, Edoardo and Martins dos Santos, Vitor A.P. and Suarez-Diez, Maria and Schaap, Peter J.

#### When referring to the SAPP ontology please cite

    Interoperable genome annotation with GBOL, an extendable infrastructure for functional data mining<br>
    Jesse C.J. van Dam, Jasper Jan J. Koehorst, Jon Olav Vik, Peter J. Schaap, Maria Suarez-Diez<br>
    bioRxiv 184747; doi: <https://doi.org/10.1101/184747>
