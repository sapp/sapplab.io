# Module Installation

Modules currently incorporated in SAPP. Help on each module can be accessed by executing the following command: `java -jar SAPP-2.0.jar`. Some modules require additional dependencies such as databases or binaries for execution. Please check below which are required.

## Conversion

*For the following modules no installation of dependencies is required.*

* EMBL/GenBank/GFF/Fasta to RDF
* RDF to EMBL / FASTA
* RDF to RDF (format conversion))
* SPARQL RDF files

## Genetic elements

### Prodigal

Prodigal is integrated into SAPP and does not require an installation.

### Infernal (RNA family detection)

Infernal is an implementation of covariance models (CMs), which are statistical models of RNA secondary structure and sequence consensus.

* The INFERNAL (cmscan) needs to be installed on the system by yourself check <https://github.com/EddyRivasLab/infernal> for more information
* The Rfam database needs to be available on the system by yourself check <https://rfam.org> for more information
* The Rfam.clanin file can be obtained from: <https://ftp.ebi.ac.uk/pub/databases/Rfam/CURRENT/Rfam.clanin>
* The Rfam.cm file can be obtained from: <https://ftp.ebi.ac.uk/pub/databases/Rfam/CURRENT/Rfam.cm.gz>
  * Make sure to `cmpress Rfam.cm` the file

```bash
wget http://eddylab.org/software/infernal/infernal.tar.gz
wget https://ftp.ebi.ac.uk/pub/databases/Rfam/CURRENT/Rfam.clanin
wget https://ftp.ebi.ac.uk/pub/databases/Rfam/CURRENT/Rfam.cm.gz
```

## Protein annotation

### InterProScan

The InterProScan module requires the application to be available. It can be obtained from <https://interproscan-docs.readthedocs.io/en/latest/HowToDownload.html>. Please follow the documentation accordingly.

```bash
mkdir my_interproscan
cd my_interproscan
wget https://ftp.ebi.ac.uk/pub/software/unix/iprscan/5/5.66-98.0/interproscan-5.66-98.0-64-bit.tar.gz
wget https://ftp.ebi.ac.uk/pub/software/unix/iprscan/5/5.66-98.0/interproscan-5.66-98.0-64-bit.tar.gz.md5

# Recommended checksum to confirm the download was successful:
md5sum -c interproscan-5.66-98.0-64-bit.tar.gz.md5
# Must return *interproscan-5.66-98.0-64-bit.tar.gz: OK*
# If not - try downloading the file again as it may be a corrupted copy.

# Uncompress
tar -pxvzf interproscan-5.66-98.0-*-bit.tar.gz

# Index hmm models
python3 setup.py -f interproscan.properties
```

### Blast (diamond)

The BLAST functionality of SAPP is currently only available for `-diamond`. DIAMOND is a sequence aligner for protein and translated DNA searches, designed for high performance analysis of big sequence data. The program can be obtained from <https://github.com/bbuchfink/diamond>.

### EggNOG

EggNOG-mapper is a tool for fast functional annotation of novel sequences. It uses precomputed orthologous groups and phylogenies from the eggNOG database (<http://eggnog5.embl.de>) to transfer functional information from fine-grained orthologs only.

Eggnog Mapper can be installed via <https://github.com/eggnogdb/eggnog-mapper/wiki>. Please read the instructions carefully.

### KofamScan

```bash
wget ftp://ftp.genome.jp/pub/db/kofam/profiles.tar.gz
wget ftp://ftp.genome.jp/pub/db/kofam/ko_list.gz
```