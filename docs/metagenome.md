## Genome annotation

!!! note
	It is possible that some modules might not work on your operating system. This is something we unfortunatly not always can resolve. If you encounter this please try to use the docker image as is provided from the docker section. All modules have been tested and should work on Ubuntu.
	
In this example we show how to annotate a genome or sequence of interest. First we need to obtain the sequence **(feel free to use your own genome fasta file)**. The genome we use in this tutorial is a *Streptococcus pyogenes MGAS5005* obtained from [ENA](https://www.ebi.ac.uk/ena/data/view/GCA_000011765.2) and the sequence file (StrepPyogenes5005.fasta) can be obtained [here](https://www.ebi.ac.uk/ena/data/view/CP000017.2&display=fasta&download=fasta&filename=StrepPyogenes5005.fasta). Please note that some browsers (safari) might change the file extension to .txt and this should be removed for this tutorial. The results of the conversion and annotation of StrepPyogenes5005.fasta can be found [here (hdt)](https://sapp.gitlab.io/files/StrepPyogenes5005_prodigal_enzdp.hdt) or [here (ttl)](https://sapp.gitlab.io/files/StrepPyogenes5005_prodigal_enzdp.hdt.ttl.gz)

###Getting help for a module
Type `java -jar <module name>.jar` to obtain the help information for a given module. For example for the  [Conversion.jar](http://download.systemsbiology.nl/sapp/Conversion.jar)  module: 

    java -jar Conversion.jar

	Usage: <main class> [options]
	  Options:    --help               (default: true)
	    -embl2rdf           input format is embl (default: false)
	    -fasta2rdf          input format is fasta (default: false)
	    -genbank2rdf        input format is genbank (default: false)
	    -gff2rdf            input format is GFF3 to create GBOL files (default: false)
	    -gtf2rdf            input format is GTF to create GBOL files (default: false)
	    -hdt2rdf, -rdf2hdt  Conversion of RDF to HDT or HDT to RDF (default: false)
	    -json2rdf           convert framed json to RDF (default: false)
	    -merge              Merging of HDT files (default: false)
	    -rdf2embl           input format is RDF to create EMBL files (default: false)
	    -rdf2fasta          input format is fasta (default: false)
	    -rdf2gtf            input format is RDF to create GTF files (default: false)
	    -rdf2json           convert from RDF to framed json (default: false)
	    -rdf2yaml           convert from RDF to framed yaml (mimicks genbank format) (default: false)
	    -yaml2rdf           convert from framed yaml (mimicks genbank format) to RDF (default: false)


###Converting your genome
---

After downloading we convert the genome sequence into the Semantic format SAPP uses. To do so we need to obtain the conversion module from [http://download.systemsbiology.nl/sapp/](http://download.systemsbiology.nl/sapp/).

Once the [Conversion.jar](http://download.systemsbiology.nl/sapp/Conversion.jar) has been downloaded you can start it from the command line. As we are currently have a FASTA file we need the `-fasta2rdf` option. No additional options will result in another help screen.

	java -jar Conversion.jar -fasta2rdf
	
	The following options are required: -i, -input -id, -identifier -org, -organism -o, -output 
	Usage: <main class> [options]
	  Options:    --help             (default: false)
	    -chromosome       Genome fasta file consists of complete chromosomes (default: false)
	    -codon            Codon table used for translating the genes (default: 0)
	    -contig           Genome fasta file consists of contigs (default: false)
	    -debug            Debug mode (default: false)
	    -fasta2rdf        Fasta to RDF conversion (default: true)
	    -format           hdt or ttl output format (default: hdt)
	    -gene             Fasta file consists of genes (default: false)
	    -genome           Fasta file consists of genome sequences (default: false)
	  * -i, -input        input FASTA file
	  * -id, -identifier  Unique identifier for your sample (e.g. GCA_000001
	  * -o, -output       output file in HDT GBOL format
	  * -org, -organism   Organism name
	    -protein          Fasta file consists of proteins (default: false)
	    -scaffold         Genome fasta file consists of scaffolds (default: false)
	    -stopcodon        Will not raise an exception when stop codons are detected during translation (default: false)
	    -topology         Describing wether sequence is circular or linear (only applicable for genome fasta files) (default: linear)
	    -translate        Translate the input sequence to protein (default: false)
	
	  * required parameter

This gives us again many options but specific to fasta conversion. The obligatory parameters (-i, -input -id, -identifier -org, -organism , -output) are required for all fasta conversions (genome, gene, protein). However some are optional and only required for specific conversion.

As we have a genome fasta file we use the following command

	java -jar Conversion.jar -fasta2rdf -input StrepPyogenes5005.fasta -o StrepPyogenes5005.hdt -genome -chromosome -id StrepPyogenes5005 -org "Streptococcus pyogenes MGAS5005"
	
	2017-10-03 08:58:11 INFO  Logger:72 - Now performing FASTA analysis
	Saving to file...
	NTRIPLES: /var/folders/l8/0x84h0pj5x96k72s3nnz3vrc0000gn/T/tmp9398597070751816026.nt
	File converted in: 528 ms 834 us
	HDTSAVE: StrepPyogenes5005.hdt

The `StrepPyogenes5005.hdt` should now be available in your folder and is the RDF database of the converted fasta file.

### Predicting genes
----

To populate the database further with for example gene predictions we can download the `genecaller.jar` from the [download page](http://download.systemsbiology.nl/sapp/)

To start the gene prediction program we start it up with the `java -jar` command.

	java -jar genecaller.jar 
	--help
	Usage: <main class> [options]
	  Options:    --help          (default: true)
	    -a, -augustus  Augustus for gene prediction (default: false)
	    -n, -naive     Naive approach for gene prediction (default: false)
	    -p, -prodigal  Prodigal for gene prediction (default: false)
	    
We currently support 2 different gene prediction programs and a naive translation of the genome sequence. As this is a bacterial genome we use prodigal.

	java -jar genecaller.jar -prodigal
	
	The following options are required: -o, -output -c, -codon -i, -input 
	Usage: <main class> [options]
	  Options:    --help        (default: false)
	    -augustus    Running in Augustus mode (default: false)
	  * -c, -codon   Codon table used... (default: 0)
	    -debug       Debug mode (default: false)
	    -f, -format  output in ttl or hdt format (default: hdt)
	  * -i, -input   Input file
	    -m, -meta    meta genome analysis (default: false)
	    -naive       Running in naive translation mode (default: false)
	  * -o, -output  Output file
	    -prodigal    Running in Prodigal mode (default: true)
	    -s, -single  single genome analysis (default: false)
	
	  * required parameter


Once the commands are filled in the gene prediction should be finished relatively fast.

	java -jar genecaller.jar -prodigal -i StrepPyogenes5005.hdt -o StrepPyogenes5005_prodigal.hdt -single -codon 11

	2017-10-03 09:03:13 INFO  Logger:23 - Connecting to HDT file
	Could not read .hdt.index, Generating a new one.
	Predicate Bitmap in 6 ms 775 us
	Count predicates in 8 ms 798 us
	Count Objects in 3 ms 422 us Max was: 916
	Bitmap in 1 ms 915 us
	Object references in 7 ms 539 us
	Sort object sublists in 4 ms 353 us
	Count predicates in 1 ms 700 us
	Index generated in 19 ms 330 us
	binaries/mac/prodigal/2.6.3/prodigal
	/Users/jasperkoehorst/Downloads/temp/prodigal
	Creating file: /Users/jasperkoehorst/Downloads/temp/prodigal
	/Users/jasperkoehorst/Downloads/temp/prodigal -q -f gff  -o StrepPyogenes5005.hdt_genecaller.gff -a StrepPyogenes5005.hdt_proteins.fasta -p single -g 11 -t StrepPyogenes5005.hdt_genecaller.trn -d StrepPyogenes5005.hdt_nucleotide.fasta -i StrepPyogenes5005.hdt.fasta
	/Users/jasperkoehorst/Downloads/temp/prodigal -q -f gff -o StrepPyogenes5005.hdt_genecaller.gff -a StrepPyogenes5005.hdt_proteins.fasta -p single -g 11 -t StrepPyogenes5005.hdt_genecaller.trn -d StrepPyogenes5005.hdt_nucleotide.fasta -i StrepPyogenes5005.hdt.fasta
	2017-10-03 09:03:19 INFO  Logger:106 - Building gene dictionary
	2017-10-03 09:03:19 INFO  Logger:125 - Building protein dictionary
	HDT2RDF: /var/folders/l8/0x84h0pj5x96k72s3nnz3vrc0000gn/T/prodigal2.ttl
	2017-10-03 09:03:30 INFO  Logger:106 - Saving file....
	Merging and saving to file...
	NTRIPLES: /var/folders/l8/0x84h0pj5x96k72s3nnz3vrc0000gn/T/tmp11822000920221641174.nt
	File converted in: 951 ms 337 us
	HDTSAVE: StrepPyogenes5005_prodigal.hdt
	
And now you should be able to see the `StrepPyogenes5005_prodigal.hdt` file which contains the original data and the newly predicted genes with additional information.

!!! info
	The sentence 	`Could not read .hdt.index, Generating a new one.` indicates no error.


### Functionally annotate enzymes using EnzDP
---

To functional annotate the genes with for example enzymes we can use the EnzDP module. This module needs to be retrieved from the download website and can be started directly. Please note that this module only works on linux systems. As the binaries from EnzDP are compiled for linux operating systems.

	java -jar EnzDP.jar 
	
	The following options are required: -i, -input -o, -output 
	Usage: <main class> [options]
	  Options:    -debug       Debug mode (default: false)
	  * -i, -input   Input HDT file
	  * -o, -output  Output HDT file
	
	  * required parameter

As this is a fully automated program only an input and output is required. Thus we run the program as followed:

	java -jar EnzDP.jar -input StrepPyogenes5005_prodigal.hdt -output StrepPyogenes5005_prodigal_enzdp.hdt
	
As it is a program which requires the EnzDP database dependency it will download the program once it is started with input and output arguments. As it is relatively large (1.9G zipped) it might take a while depending on your internet connection.

The database will be placed next to the jar file inside the `./Dependencies/Database/EnzDP` folder and is currently 11.68GB.

After downloading it will automatically continue and run the analysis.

	....
	NTRIPLES: /tmp/tmp1383585639358114106.nt
	HDT2RDF: /tmp/tmp1383585639358114106.nt
	File converted in: 1 sec 935 ms 378 us
	HDTSAVE: StrepPyogenes5005_prodigal_enzdp.hdt
	

Now you should have a file called `StrepPyogenes5005_prodigal_enzdp.hdt` which contains the genome sequence, the predicted genes and the functional annotation from EnzDP in a single semantic database.

## Now you can move to the Query section to extract features from the database
